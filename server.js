var http=require("http");
var finalhandler=require("finalhandler");
var serveStatic=require("serve-static");

// declare what port we'd like to listen on
// this should be configurable by an external file
// AGMLjs?
var port=8080;

// make a function for serving static files from a directory
var serve=serveStatic('www',{
    index:['template.html']
});

// tell the user what you're doing
console.log("Launching a server on port %s",port);

http.createServer(function(req,res){
    // if you're really serious about checking who's visiting, you need to be more careful than this
    // visitors can set whatever headers they want
    // if you are behind a proxy, then x-forwarded-for will tell you the visitor's ip
      // while remoteAddress will be that of your proxy
    // if you are not, then they could set whatever ip they want in x-forwarded-for
      // while remoteAddress would tell the truth
    // remain vigilant!
    var visitorIP=req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    // make some noise on the console if you get a visitor
    console.log('we have a visitor: %s',visitorIP);
    // cook up a callback
    var done=finalhandler(req,res);

    // serve, or 404, as the case may be
    serve(req,res,done);
}).listen(port);
