# cjdns-web

## Introduction

cjdns-web is a web frontend to the cjdns networking program written by cjd.

This software is currently in active development and doesn't function yet. Most, if not all, html files in the www directory are mockups and subject to change.

This software is licensed under the GPLv3.

## Interface Goals
1. Provide a functional and easy-to-use web interface to manage the cjdns software.
2. Put commonly used cjdns functions at the forefront and make them easily accessible and navigable.
3. Be visually appealing.

## Implementation Goals
1. Communicate with cjdns using the existing API and node.js.
2. Be secure enough to operate from a remote location over the clearweb.
3. Not use root access.
4. Not start, restart, or stop cjdns directly
5. Feed peers and information to an already running instance of cjdns
6. Be able to add peers to cjdns in real time without the need of restarting it (RPC?)
7. ... (more to come)

## Likely dependencies
- A standalone database server (possibly will implement a built in one)
- Node.js
- cjdns

## Installation

Assuming you're running some kind of Linux-based system...

You will need a recent version of nodejs and npm. For Ubuntu, that means doing something like [this](http://askubuntu.com/questions/49390/how-do-i-install-the-latest-version-of-node-js).

Fetch the repository:

`git clone https://github.com/drsn0w/cjdns-web`

Change into the directory:

`cd cjdns-web`

Install nodejs packages:

`npm install`

Launch the server:

`node server.js`

Browse to the server at http://127.0.0.1:8080

